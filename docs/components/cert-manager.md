# Component: cert-manager

This component is responsible for installing and configuring cert-manager in the
`openshift-cert-manager` namespace.
- cert-manager Operator for Red Hat OpenShift

## Part Of
- Component: `cert-manager`
- Tags: `cert-manager`
- Application: `cert-manager`

# Index

<!-- vim-markdown-toc GFM -->

* [cert-manager Variables](#cert-manager-variables)
* [cert-manager Details](#cert-manager-details)

<!-- vim-markdown-toc -->

# cert-manager Variables


# OpenShift cert-manager  Operator Variables

| Option | Required/Optional | Comments | Default |
|--------|-------------------|----------|---------|
| openshift_cert_manager_operator_channel | Optional | The subscription channel to use | `stable-v1` |

# cert-manager Details

All of the configuration for this component is pulled from the upstream base. It does the following things:

## cert-manager
- Create a namespace for the OpenShift cert-manager Operator (`openshift-cert-manager`).
- Create an OperatorGroup in the `openshift-cert-manager` namespace.
- Create a subscription for the OpenShift cert-manager Operator

See https://cert-manager.io/docs/configuration/ for documentation how to configure cert-manager.
